﻿using AlumniNetworkBackendAPI.Models;
using AlumniNetworkBackendAPI.Services.Abstract;
using AlumniNetworkBackendAPI.Services.Concrete;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System.Reflection;

namespace AlumniNetworkBackendAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
              .AddJwtBearer(options =>
              {
                  options.TokenValidationParameters = new TokenValidationParameters
                  {
                      //Access token for postman can be found at http://localhost:8000/#
                      //requires token from keycloak instance - location stored in secret manager
                      IssuerSigningKeyResolver = (token, securityToken, kid, parameters) =>
                      {
                          var client = new HttpClient();
                          var keyuri = Configuration["TokenSecrets:KeyURI"];
                          //Retrieves the keys from keycloak instance to verify token
                          var response = client.GetAsync(keyuri).Result;
                          var responseString = response.Content.ReadAsStringAsync().Result;
                          var keys = JsonConvert.DeserializeObject<JsonWebKeySet>(responseString);
                          return keys.Keys;
                      },

                      ValidIssuers = new List<string>
                      {
                            Configuration["TokenSecrets:IssuerURI"]
                      },

                      //This checks the token for a the 'aud' claim value
                      ValidAudience = "account",
                  };
              });

            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            services.AddDbContext<AlumniNetworkDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Services below
            services.AddScoped<IGroupService, GroupService>();
            services.AddScoped<IEventService, EventService>();
            services.AddScoped<ITopicService, TopicService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPostService, PostService>();


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "AlumniNetwork API",
                    Version = "v1",
                    Description = "More info here",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "More Info Here",
                        Email = "Mail",
                        Url = new Uri("https://www.noroff.no/accelerate"),

                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under MIT",
                        Url = new Uri("https://opensource.org/licenses/MIT"),
                    }
                });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {

            }
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "AlumniNetwork API v1");
                    //c.RoutePrefix = string.Empty;
                });

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }
    }
}
