﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.Event;
using AlumniNetworkBackendAPI.Services.Abstract;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Text.RegularExpressions;

namespace AlumniNetworkBackendAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : Controller
    {
        private readonly IEventService _eventService;
        private readonly IMapper _mapper;
        public EventController(IEventService eventService, IMapper mapper)
        {
            _eventService = eventService;
            _mapper = mapper;
        }

        /// <summary>
        /// Makes a request for events based on userId
        /// </summary>
        /// <param name="userId">Id of user to get the correct events its attached to</param>
        /// <returns>Returns a list of events for the current user logged in</returns>
        [HttpGet("/event/user/{userId}")]
        public async Task<ActionResult<EventReadDTO>> GetAllEventsBasedOnUserIdAsync(int userId)
        {
            var events = await _eventService.GetAllEventsBasedOnUserIdAsync(userId);
            return Ok(_mapper.Map<List<EventReadDTO>>(events));
        }

        /// <summary>
        /// Makes a request for events based on groupId
        /// </summary>
        /// <param name="groupId">Id of group to get the correct events for the group</param>
        /// <returns>Returns a list of events based on group</returns>
        [HttpGet("/event/group/{groupId}")]
        public async Task<ActionResult<EventReadDTO>> GetAllGroupEvents(int groupId)
        {
            var groupEvents = await _eventService.GetAllEventsBasedOnGroup(groupId);
            return Ok(_mapper.Map<List<EventReadDTO>>(groupEvents));
        }

        /// <summary>
        /// Makes a request for a specific event
        /// </summary>
        /// <param name="eventId">Id to call the correct event</param>
        /// <returns>Returns a event depending on eventId</returns>
        [HttpGet("/event/{eventId}")]
        public async Task<ActionResult<EventReadDTO>> GetEventsBasedOnEventId(int eventId)
        {
            var eventById = await _eventService.GetEventsBasedOnId(eventId);
            return Ok(_mapper.Map<List<EventReadDTO>>(eventById));
        }

        /// <summary>
        /// Makes a get request for events
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Returns a list of events depending on parameters</returns>
        [HttpGet("/event/subscribed/{userId}")]
        public async Task<ActionResult<EventReadDTO>> GetSubscribedEventsForUserAsync(int userId)
        {
            var events = await _eventService.GetSubscribedEventsForUserAsync(userId);
            
                return Ok(_mapper.Map<List<EventReadDTO>>(events));
        }

        /// <summary>
        /// Creates an new event
        /// </summary>
        /// <param name="eventDTO">Schema for creating an event</param>
        /// <returns>Returns a string that lets the user know the event was successfully created</returns>
        [HttpPost("/event")]
        [ActionName(nameof(AddEventAsync))]
        public async Task<ActionResult<EventCreateDTO>> AddEventAsync(EventCreateDTO eventDTO)
        {
            var newEvent = _mapper.Map<Event>(eventDTO);
            newEvent = await _eventService.AddEventAsync(newEvent);
            return CreatedAtAction(nameof(AddEventAsync), new { id = newEvent.Id }, eventDTO);
        }

        /// <summary>
        /// Updates an specific event
        /// </summary>
        /// <param name="id">Id of the event to check if it exist before trying to apply changes</param>
        /// <param name="eventDTO">Schema for updating an event</param>
        /// <returns>Returns a string that lets the user know the event was successfully updated</returns>
        [HttpPut("/event/{id}")]
        public async Task<IActionResult> UpdateEventAsync(int id, EventUpdateDTO eventDTO)
        {

            if (!_eventService.EventExists(id)) return NotFound();

            var events = _mapper.Map<Event>(eventDTO);
            await _eventService.UpdateEventAsync(events);

            return Ok("Event has been updated!");
        }

        /// <summary>
        /// Sending an invitation to the selected group
        /// </summary>
        /// <param name="groupId">Id of the selected group to receive the invite</param>
        /// <param name="eventId">Id of the event to check if it exist before sending the invite</param>
        /// <returns>Returns a string that the group has been invited to the event else if the event doesnt exist it will return a message it couldnt find the event</returns>
        [HttpPost("/event/{eventId}/invite/group/{groupId}")]
        public async Task<IActionResult> InviteGroupToEventAsync(int groupId, int eventId)
        {
            if (!_eventService.EventExists(eventId))
            {
                return NotFound($"Couldnt find the event with the id: {groupId} *SadFace*");
            }
            await _eventService.InviteGroupToEventAsync(groupId, eventId);
            return Ok("Group has been invited to the event!");
        }

        /// <summary>
        /// Removes the invitation for the group from event
        /// </summary>
        /// <param name="groupId">Id for the group which shall has its invitation removed</param>
        /// <param name="eventId">Id for which event its getting removed from</param>
        /// <returns>Returns a string that let the user know the invitation towards the group has been removed</returns>
        [HttpDelete("/event/{eventId}/invite/group/{groupId}")]
        public async Task<IActionResult> RemoveInviteGroupAsync(int groupId, int eventId)
        {
            if (!_eventService.EventExists(eventId))
            {
                return NotFound();
            }

            await _eventService.RemoveInviteGroupAsync(groupId, eventId);

            return Ok("Selected Event invite has been deleted!");
        }

        /// <summary>
        /// Sending an invitation to the selected user
        /// </summary>
        /// <param name="userId">Id of the selected user to receive the invite</param>
        /// <param name="eventId">Id of the event to check if it exist before sending the invite</param>
        /// <returns>Returns a string that the user has been invited to the event else if the event doesnt exist it will return a message it couldnt find the event</returns>
        [HttpPost("/event/{eventId}/invite/user/{userId}")]
        public async Task<IActionResult> InviteUserToEventAsync(int userId, int eventId)
        {
            if(!_eventService.EventExists(eventId))
            {
                return NotFound($"Couldnt find the event with the id: {userId} *SadFace*");
            }
            await _eventService.InviteUserToEventAsync(userId, eventId);
            return Ok("User has been invited to the event!");
        }

        /// <summary>
        /// Removes the invitation for the user from event
        /// </summary>
        /// <param name="userId">Id for the user which shall has its invitation removed</param>
        /// <param name="eventId">Id for which event its getting removed from</param>
        /// <returns>Returns a string that let the user know the invitation towards the group has been removed</returns>
        [HttpDelete("/event/{eventId}/invite/user/{userId}")]
        public async Task<IActionResult> RemoveInviteUserAsync(int userId, int eventId)
        {
            if (!_eventService.EventExists(eventId))
            {
                return NotFound();
            }

            await _eventService.RemoveInviteGroupAsync(userId, eventId);

            return Ok("Selected Event invite has been deleted!");
        }

        /// <summary>
        /// Sending an invitation to the selected topic
        /// </summary>
        /// <param name="topicId">Id of the selected topic to receive the invite</param>
        /// <param name="eventId">Id of the event to check if it exist before sending the invite</param>
        /// <returns>Returns a string that the group has been invited to the topic else if the event doesnt exist it will return a message it couldnt find the event</returns>
        [HttpPost("/event/{eventId}/invite/topic/{topicId}")]
        public async Task<IActionResult> InviteTopicToEventAsync(int topicId, int eventId)
        {
            if (!_eventService.EventExists(eventId))
            {
                return NotFound($"Couldnt find the event with the id: {topicId} *SadFace*");
            }
            await _eventService.InviteTopicToEventAsync(topicId, eventId);
            return Ok("Topic has been invited to the event!");
        }

        /// <summary>
        /// Sending an invitation to the selected topic
        /// </summary>
        /// <param name="topicId">Id of the selected topic to receive the invite</param>
        /// <param name="eventId">Id of the event to check if it exist before sending the invite</param>
        /// <returns>Returns a string that let the user know the invitation towards the group has been removed</returns>
        [HttpDelete("/event/{eventId}/invite/topic/{topicId}")]
        public async Task<IActionResult> RemoveInviteTopicAsync(int topicId, int eventId)
        {
            if (!_eventService.EventExists(eventId))
            {
                return NotFound();
            }

            await _eventService.RemoveInviteGroupAsync(topicId, eventId);

            return Ok("Selected Event invite has been deleted!");
        }

        /// <summary>
        /// RSVP check to see how many user actually attended an event
        /// </summary>
        /// <param name="eventId">Id that specifices which event</param>
        /// <param name="userId">Id to show which user attended the event</param>
        /// <returns></returns>
        [HttpPost("/event/{eventId}/rsvp")]
        public async Task<IActionResult> RSVPRecordPost(int eventId, int userId)
        {
            await _eventService.RSVPRecordPost(eventId, userId);

            return Ok("It Works!");
        }

    }

}
