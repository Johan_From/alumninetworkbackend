﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.Topic;
using AlumniNetworkBackendAPI.Services.Abstract;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace AlumniNetworkBackendAPI.Controllers
{
    //[Authorize]
    [Route("[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class TopicController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ITopicService _topicService;

        public TopicController(IMapper mapper, ITopicService topicService)
        {
            _mapper = mapper;
            _topicService = topicService;
        }

        /// <summary>
        /// Gets Topics based on parameters if specified, if no parameters specified it returns the entire list of topics
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet("/topic/")]
        public async Task<ActionResult<TopicReadDTO>> GetAllTopicsAsync(string? searchString = null, int? limit = null)
        {
            return Ok(_mapper.Map<List<TopicReadDTO>>(await _topicService.GetAllTopicsAsync(searchString, limit)));
        }

        /// <summary>
        /// Gets one Topic based on id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a topic based on id</returns>
        [HttpGet("/topic/{id}")]
        public async Task<ActionResult<TopicReadDTO>> GetTopicByIdAsync(int id)
        {
            var topic = await _topicService.GetTopicByIdAsync(id);
            if(topic == null)
            {
                return NoContent();
            }

            return Ok(_mapper.Map<List<TopicReadDTO>>(await _topicService.GetTopicByIdAsync(id)));
        }

        /// <summary>
        /// Assigns user to topic after the post has been created
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpPost("/topic/{userId}")]
        public async Task<ActionResult<TopicCreateDTO>> CreateTopicAndAssignSubscriber(TopicCreateDTO topic, int userId)
        {

            var domainTopic = _mapper.Map<Topic>(topic);

            //return Ok(_mapper.Map<List<TopicCreateDTO>>(await _topicService.CreateTopicAndAddCreatorAsSubscriber(domainTopic, userId)));

            domainTopic = await _topicService.AddTopic(domainTopic);

            var readTopic = CreatedAtAction("GetTopic",
                new { id = domainTopic.Id },
                _mapper.Map<TopicReadDTO>(domainTopic));

            int topicId = domainTopic.Id;

            await _topicService.CreateTopicAndAddCreatorAsSubscriber(topicId, userId);

            return Ok(readTopic);
        }

        /// <summary>
        /// Assigns user to topic based on userId and topicId
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="topicId"></param>
        /// <returns></returns>
        [HttpPost("/topic/{topicId}/join")]
        public async Task<ActionResult<TopicReadDTO>> AssignUserToTopic(int topicId, int userId)
        {
            return Ok(_mapper.Map<List<TopicReadDTO>>(await _topicService.JoinTopic(topicId, userId)));
        }

    }
}
