﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.Post;
using AlumniNetworkBackendAPI.Services.Abstract;
using AlumniNetworkBackendAPI.Services.Concrete;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Net.Mime;
using System.Text.RegularExpressions;

namespace AlumniNetworkBackendAPI.Controllers
{
    //[Authorize]
    [Route("[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class PostController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IPostService _postService;

        public PostController(IMapper mapper, IPostService postService)
        {
            _mapper = mapper;
            _postService = postService;
        }

        /// <summary>
        /// Gets all posts for user based on userId, optional parameters: searchstring and limit
        /// </summary>
        /// <param name="userId">User id determines which user data to fetch</param>
        /// <param name="searchString">Searchstring which determines which user data to fetch</param>
        /// <param name="limit">Limit sets the amount of posts to fetch</param>
        /// <returns>Returns a list of posts depending on the parameters</returns>
        [HttpGet("/post")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPosts([Required] int userId, string? searchString, int? limit)
        {

            var domainPostList = _mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostsAsync(userId, searchString, limit));

            if (domainPostList.Count > 0)
            {
                return Ok(domainPostList);
            }
            if (domainPostList.Count == 0)
            {
                return StatusCode(StatusCodes.Status404NotFound);
            }
            return Ok(_mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostsAsync(userId, searchString, limit)));
        }

        /// <summary>
        /// Gets all posts as direct messages between user
        /// </summary>
        /// <param name="userId">Id for the user</param>
        /// <param name="searchString">Searchstring which determines which user posts to fetch</param>
        /// <param name="limit">Limit sets the amount of posts to fetch</param>
        /// <returns>Returns a list of direct messages between user</returns>
        [HttpGet("/post/user")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostsBetweenUsers([Required] int userId, string? searchString, int? limit)
        {
            return Ok(_mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostBetweenUserAsync(userId, searchString, limit)));
        }


        /// <summary>
        /// Gets all the posts between two specific users
        /// </summary>
        /// <param name="userId">Id for the current logged in user</param>
        /// <param name="requestingId">Id for selected user</param>
        /// <param name="searchString">Searchstring which determines which user posts to fetch</param>
        /// <param name="limit">Limit sets the amount of posts to fetch</param>
        /// <returns>Returns a list of direct messages the users</returns>
        [HttpGet("/post/user/{userId}")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetSpecificPostBetweenUsers([Required] int userId, [Required] int requestingId, string? searchString, int? limit)
        {
            return Ok(_mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostsBetweenSpecificUsersAsync(userId, requestingId, searchString, limit)));
        }

        /// <summary>
        /// Gets all the posts for a specific topic based on the topic id
        /// </summary>
        /// <param name="topicId">Id for the specific topic</param>
        /// <param name="userId">Id for which user made thepost</param>
        /// <param name="searchString">Searchstring which determines which topic posts to fetch</param>
        /// <param name="limit">Limit sets the amount of posts to fetch</param>
        /// <returns>Returns a list of posts for the specific topic</returns>
        [HttpGet("/post/topic/{topicId}")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostTopics([Required] int topicId, int userId, string? searchString, int? limit)
        {
            var postTopic =  _mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostTopicsAsync(topicId, userId, searchString, limit));
            if(postTopic.Count != 0)
            {
                return Ok(postTopic);
            }
            if(postTopic.Count == 0)
            {
                return Ok(postTopic);
            }
            return Ok(_mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostTopicsAsync(topicId, userId, searchString, limit)));
        }

        /// <summary>
        /// Gets all the posts in a group based on the specific group id
        /// </summary>
        /// <param name="groupId">Id for the specific group</param>
        /// <param name="searchString">Searchstring which determines which group posts to fetch</param>
        /// <param name="limit">Limit sets the amount of posts to fetch</param>
        /// <returns>Returns a list of posts for the specific group</returns>
        [HttpGet("/post/group/{groupId}")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostGroups([Required] int groupId, string? searchString, int? limit)
        {
            var postGroup = _mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostGroupAsync(groupId, searchString, limit));
            if (postGroup.Count != 0)
            {
                return Ok(postGroup);
            }
            if (postGroup.Count == 0)
            {
                return Ok(postGroup);
            }

            return Ok(_mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostGroupAsync(groupId, searchString, limit)));
        }

        /// <summary>
        /// Method for a user to get all the posts sent to him/her
        /// </summary>
        /// <param name="userId">Id for the user</param>
        /// <returns>Returns a list of posts to the user that they have recieved</returns>
        [HttpGet("/post/direct/user/{userId}")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetDirectUserPost([Required] int userId)
        {
            var directPost = _mapper.Map<List<PostReadDTO>>(await _postService.GetAllDirectPosts(userId));
            if (directPost.Count != 0)
            {
                return Ok(directPost);
            }
            if (directPost.Count == 0)
            {
                return Ok(directPost);
            }

            return directPost;
        }

        /// <summary>
        /// Gets all posts in a event based on specific event id
        /// </summary>
        /// <param name="eventId">Id for the specific event</param>
        /// <param name="userId">Current user thats logged in</param>
        /// <param name="searchString">Searchstring which determines which group posts to fetch</param>
        /// <param name="limit">Limit sets the amount of posts to fetch</param>
        /// <returns>Returns a list of posts for a specific event</returns>
        [HttpGet("/post/event/{eventId}")]
        public async Task<ActionResult<IEnumerable<PostReadDTO>>> GetAllPostEvents([Required] int eventId, int userId, string? searchString, int? limit)
        {

            var postEvent = _mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostEventAsync(eventId, userId, searchString, limit));
            if (postEvent.Count != 0)
            {
                return Ok(postEvent);
            }
            if (postEvent.Count == 0)
            {
                return Ok(postEvent);
            }


            return Ok(_mapper.Map<List<PostReadDTO>>(await _postService.GetAllPostEventAsync(eventId, userId, searchString, limit)));
        }

        /// <summary>
        /// Creates a new post
        /// </summary>
        /// <param name="post">post object which is beeing added</param>
        /// <param name="userId">userid which is beeing passed as parameter</param>
        /// <returns>Saves the new post object in the database</returns>
        [HttpPost("/post")]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<PostCreateDTO>> CreatePost(PostCreateDTO post, int userId)
        {
            var domainPost = _mapper.Map<Post>(post);

            domainPost = await _postService.AddPostAsync(domainPost, userId);

            if(domainPost != null)
            {
                var readPost = CreatedAtAction("GetPost",
                    new { id = domainPost.Id },
                    _mapper.Map<PostReadDTO>(domainPost));

                    return Ok(readPost);
            }
            if(domainPost == null)
            {
                return StatusCode(StatusCodes.Status403Forbidden);
            }

            return NoContent();

        }

        /// <summary>
        /// Updates post
        /// </summary>
        /// <param name="postId">postid which specifies which post to update</param>
        /// <param name="post">post object which is beeing updated</param>
        /// <returns>Saves the updated post object in the database</returns>
        [HttpPut("/post/{postId}")]
        public async Task<ActionResult<PostEditDTO>> UpdatePost(int postId, PostEditDTO post)
        {
            if(!_postService.PostExists(postId))
            {
                return NotFound();
            }

            if (postId != post.Id)
            {
                return BadRequest();
            }

            // If character exists
            
            var domainPost = _mapper.Map<Post>(post);
            await _postService.UpdatePostAsync(postId, domainPost);


            return NoContent();
        }

        /// <summary>
        /// Get the post based on reply Id
        /// </summary>
        /// <param name="postReplyId">Id for the post reply</param>
        /// <returns>Returns the post based on reply Id</returns>
        [HttpGet("/posts/replies/{postReplyId}")]
        public async Task<ActionResult<PostReadDTO>> GetPostRepliesById( int postReplyId)
        {
            return Ok(_mapper.Map<List<PostReadDTO>>(await _postService.GetPostRepliesBasedOnId(postReplyId)));
        }
    }
}
