﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.User;
using AlumniNetworkBackendAPI.Services.Abstract;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;

namespace AlumniNetworkBackendAPI.Controllers
{
    //[Authorize]
    [Route("[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class UserController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UserController(IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }

        /// <summary>
        /// Gets all the users from the database
        /// </summary>
        /// <returns>Returns a list of users</returns>
        [HttpGet]
        [Route("/users")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetAllUsers()
        {
            var users = await _userService.GetAllUsersAsync();
            if(users == null)
            {
                return NoContent();
            }

            return Ok(_mapper.Map<List<UserReadDTO>>(users));
        }

        /// <summary>
        /// Gets a specific user from the database
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Returns a specific user depending on userId</returns>
        [HttpGet("/user/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<User>> GetOneUser(int userId)
        {
            var user = await _userService.GetSpecificUserAsync(userId);

            if(user == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<UserReadDTO>(user));
        }

        /// <summary>
        /// Gets one user based on their email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Returns a user based on email</returns>
        [HttpGet("/user/email/{email}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<User>> GetOneUserBasedOnEmail(string email)
        {
            var user = await _userService.GetSpecificUserBasedOnEmailAsync(email);

            if(user == null)
            {
                return NoContent();
            }

            return Ok(_mapper.Map<UserReadDTO>(user));
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <param name="dtoUser"></param>
        /// <returns>Sends and saves the new user object to the database</returns>
        [HttpPost("/user")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<UserReadDTO>> PostUser(UserCreateDTO dtoUser)
        {
            var domainUser = _mapper.Map<User>(dtoUser);

            domainUser = await _userService.AddUserAsync(domainUser);

            var readUser = CreatedAtAction("GetUser",
                new { id = domainUser.Id },
                _mapper.Map<UserReadDTO>(domainUser));

            return Ok(readUser);
        }

        /// <summary>
        /// Updates a specific user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="dtoUser"></param>
        /// <returns>Sends and saves the updated user object to the database</returns>
        [HttpPatch("/user/userId")]
        public async Task<IActionResult> PatchUser(int userId, UserEditDTO dtoUser)
        {
            if(userId != dtoUser.Id)
            {
                return BadRequest();
            }

            if (!_userService.UserExists(userId))
            {
                return NotFound();
            }

            var domainUser = _mapper.Map<User>(dtoUser);
            await _userService.UpdateUserAsync(domainUser);

            return NoContent();
        }
    } 
}
