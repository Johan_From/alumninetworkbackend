﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.Group;
using AlumniNetworkBackendAPI.Services.Abstract;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net.Mime;

namespace AlumniNetworkBackendAPI.Controllers
{
    //[Authorize]
    [Route("[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class GroupController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IGroupService _groupService;

        public GroupController(IMapper mapper, IGroupService groupService)
        {
            _mapper = mapper;
            _groupService = groupService;
        }

        /// <summary>
        /// Gets all the groups from the database with optional parameters
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="limit"></param>
        /// <returns>Returns groups depending on the searchstring and limit</returns>
        [HttpGet]
        [Route("/group/searchString/limit")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroups(string? searchString, int? limit)
        {
            // Pagination or limit?
            var groups = await _groupService.GetAllGroupsAsync(searchString, limit);
            if(groups == null)
            {
                return NoContent();
            }

            return Ok(_mapper.Map<List<GroupReadDTO>>(groups));   
        }

        /// <summary>
        /// Gets a specific group from the database
        /// </summary>
        /// <param name="groupId">Id for the specific group</param>
        /// <returns>Returns a group depending on groupId</returns>
        [HttpGet("/group/{groupId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Group>> GetOneGroup(int groupId)
        {
            var group = await _groupService.GetSpecificGroupAsync(groupId);

            if(group == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<GroupReadDTO>(group));
        }

        /// <summary>
        /// Creates a new group and adds it to the database
        /// </summary>
        /// <param name="dtoGroup">Group object</param>
        /// <param name="userId">Id for which user that created the group</param>
        /// <returns>Saves the group object to the database</returns>
        [HttpPost("/group")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<GroupReadDTO>> PostGroup(GroupCreateDTO dtoGroup, [FromQuery] int[] userId)
        {
            var domainGroup = _mapper.Map<Group>(dtoGroup);
            
            domainGroup = await _groupService.AddGroupAsync(domainGroup);

            var readGroup = CreatedAtAction("GetGroup",
                new { id = domainGroup.Id },
                _mapper.Map<GroupReadDTO>(domainGroup));

            int groupId = domainGroup.Id;

            await _groupService.UpdateUserToGroupAsync(groupId, userId);


            return Ok(readGroup);
            
        }
        
        /// <summary>
        /// Joins a group
        /// </summary>
        /// <param name="groupId">Id for which group the user are attempting to join</param>
        /// <param name="userId">Id for which user that attempts to join the group</param>
        /// <returns>Adds the user to the group</returns>
        [HttpPut("/group/join/{groupId}")]
        public async Task<ActionResult<GroupReadDTO>> JoinGroup(int groupId,  int userId) 
        {
            return Ok(_mapper.Map<List<GroupReadDTO>>(await _groupService.JoinGroup(groupId, userId)));
        }

        /// <summary>
        /// Updates a group with the new provided data
        /// </summary>
        /// <param name="groupId">Id for the specific group</param>
        /// <param name="dtoGroup">Group object to be updated</param>
        /// <returns>Returns the updated group object to the database</returns>
        [HttpPut("/group/{groupId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> PutGroup(int groupId, GroupEditDTO dtoGroup)
        {
            // If id not the same
            if (groupId != dtoGroup.Id)
            {
                return BadRequest();
            }

            // If character exists
            if (!_groupService.GroupExists(groupId))
            {
                return NotFound();
            }

            var domainGroup = _mapper.Map<Group>(dtoGroup);
            await _groupService.UpdateGroupAsync(domainGroup);

            return NoContent();

        }
    }
}
