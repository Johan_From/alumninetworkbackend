﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.Post;
using AutoMapper;

namespace AlumniNetworkBackendAPI.Profiles
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<Post, PostReadDTO>()
                .ForMember(pdto => pdto.EventId, opt => opt
                .MapFrom(p => p.Event.Id))
                .ForMember(pdto => pdto.UserId, opt => opt
                .MapFrom(p => p.User.Id))
                .ForMember(pdto => pdto.TopicId, opt => opt 
                .MapFrom(p => p.Topic.Id))
                .ForMember(pdto => pdto.GroupId, opt => opt 
                .MapFrom(p => p.Group.Id))
                .ForMember(pdto => pdto.PostReplies, opt => opt
                .MapFrom(p => p.RepliedPosts.Select(x => x.Id).ToArray()))
                .ReverseMap();

            CreateMap<PostEditDTO, Post>();
            CreateMap<PostCreateDTO, Post>();

               
        }
    }
}
