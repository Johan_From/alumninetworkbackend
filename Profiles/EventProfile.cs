﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.Event;
using AlumniNetworkBackendAPI.Models.DTOs.Group;
using AutoMapper;

namespace AlumniNetworkBackendAPI.Profiles
{
    public class EventProfile : Profile
    {
        public EventProfile()
        {
            CreateMap<Event, EventReadDTO>()
            .ForMember(gdto => gdto.Users, opt => opt
                .MapFrom(c => c.Users.Select(c => c.Id).ToArray()))
            .ForMember(gdto => gdto.Groups, opt => opt
                .MapFrom(c => c.Groups.Select(c => c.Id).ToArray()))
            .ForMember(gdto => gdto.Topics, opt => opt
                .MapFrom(c => c.Topics.Select(c => c.Id).ToArray()))
            .ForMember(gdto => gdto.Posts, opt => opt
                .MapFrom(c => c.Posts.Select(c => c.Id).ToArray()));
            CreateMap<EventCreateDTO, Event>();
            CreateMap<EventUpdateDTO, Event>();

        }
    }
}
