﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.User;
using AutoMapper;

namespace AlumniNetworkBackendAPI.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>()
                .ForMember(udto => udto.Posts, opt => opt
                .MapFrom(c => c.Posts.Select(c => c.Id).ToArray()))
                .ForMember(udto => udto.Events, opt => opt
                .MapFrom(c => c.Events.Select(c => c.Id).ToArray()))
                .ReverseMap();
            CreateMap<UserCreateDTO, User>();
            CreateMap<UserEditDTO, User>();
        }
    }
}
