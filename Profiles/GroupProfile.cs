﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.Group;
using AutoMapper;

namespace AlumniNetworkBackendAPI.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<Group, GroupReadDTO>()
                .ForMember(gdto => gdto.Users, opt => opt
                .MapFrom(c => c.Users.Select(c => c.Id).ToArray()))
                .ForMember(gdto => gdto.Events, opt => opt
                .MapFrom(c => c.Events.Select(c => c.Id).ToArray()))
                .ReverseMap();
            CreateMap<GroupCreateDTO, Group>();
            CreateMap<GroupEditDTO, Group>();
        }
    }
}
