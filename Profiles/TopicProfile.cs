﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.DTOs.Topic;
using AutoMapper;

namespace AlumniNetworkBackendAPI.Profiles
{
    public class TopicProfile : Profile
    {
        public TopicProfile()
        {
            CreateMap<Topic, TopicReadDTO>()
                .ForMember(cdto => cdto.Events, opt => opt
                .MapFrom(t => t.Events.Select(t => t.Id).ToArray()))
                .ForMember(cdto => cdto.Users, opt => opt
                .MapFrom(u => u.Users.Select(u => u.Id).ToArray()))
                .ReverseMap();

            CreateMap<TopicCreateDTO, Topic>();
                //.ForMember(cdto => cdto.Users, opt => opt 
                //.MapFrom(u => u.Users.Select(u => u.Id).ToArray()))
                //.ReverseMap();
            CreateMap<TopicEditDTO, Topic>();
        }
    }
}
