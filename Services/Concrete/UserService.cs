﻿using AlumniNetworkBackendAPI.Models;
using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Services.Abstract;
using Microsoft.EntityFrameworkCore;

namespace AlumniNetworkBackendAPI.Services.Concrete
{
    public class UserService : IUserService
    {
        private readonly AlumniNetworkDbContext _context;

        public UserService(AlumniNetworkDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Methods that adds a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Saves and sends the user object to the database</returns>
        public async Task<User> AddUserAsync(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return user;
        }

        /// <summary>
        /// Method that gets all the users
        /// </summary>
        /// <returns>Returns a list of all users</returns>
        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
           return await _context.Users.Include(x => x.Posts).Include(x => x.Events).ToListAsync();  
        }

        /// <summary>
        /// Method that gets a specific user
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a user depending on id</returns>
        public async Task<User> GetSpecificUserAsync(int id)
        {
            return await _context.Users.Include(x => x.Posts).Include(x => x.Events).Where(x => x.Id == id).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Method to get a specifc user based on email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Returns a user based on email</returns>
        public async Task<User> GetSpecificUserBasedOnEmailAsync(string email)
        {
            return await _context.Users.Include(x => x.Posts).Include(x => x.Events).Where(x => x.Email == email).FirstOrDefaultAsync();
        }

        /// <summary>
        /// Method that updates a user
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Saves and sends the user object to the database</returns>
        public async Task UpdateUserAsync(User user)
        {
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Boolean method that checks if user exists based on id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool UserExists(int id)
        {
            return _context.Users.Any(x => x.Id == id);
        }
    }
}
