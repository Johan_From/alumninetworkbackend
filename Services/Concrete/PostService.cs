﻿using AlumniNetworkBackendAPI.Models;
using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Services.Abstract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Mvc;

namespace AlumniNetworkBackendAPI.Services.Concrete
{
    public class PostService : IPostService
    {
        private readonly AlumniNetworkDbContext _alumniNetworkDbContext;
        public PostService(AlumniNetworkDbContext alumniNetworkDbContext)
        {
            _alumniNetworkDbContext = alumniNetworkDbContext;
        }

        /// <summary>
        /// Method to get all posts
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="searchString"></param>
        /// <param name="limit"></param>
        /// <returns>Returns a list of posts depending on parameters</returns>
        public async Task<IEnumerable<Post>> GetAllPostsAsync(int userId, string? searchString, int? limit)
        {
            var user = await _alumniNetworkDbContext.Users.FindAsync(userId);

            if(searchString != null)
            {

                var listOfPosts = await _alumniNetworkDbContext.Posts
                .Where(x => x.Title.ToLower()
                .Contains(searchString.ToLower()))
                .Include(x => x.Group)
                .Include(x => x.Topic)
                .Include(x => x.RepliedPosts)
                .ToListAsync();

                return listOfPosts.Where(x => x.UserId == user.Id).OrderBy(x => x.TimeStamp).ToList();
            }

            if(limit != null)
            {
                var listOfPosts = await _alumniNetworkDbContext.Posts
                    .Include(x => x.Group)
                    .Include(x => x.Topic)
                    .Include(x => x.RepliedPosts)
                    .ToListAsync();

                return listOfPosts.OrderBy(x => x.Id)
                    .Take(limit.Value)
                    .Where(x => x.SenderId == user.Id).OrderBy(x => x.TimeStamp).ToList();
            }

            if(searchString != null && limit != null)
            {
                var listOfPosts = await _alumniNetworkDbContext.Posts
                .Where(x => x.Title.ToLower()
                .Contains(searchString.ToLower()))
                .Include(x => x.Group)
                .Include(x => x.Topic)
                .Include(x => x.RepliedPosts)
                .ToListAsync();

                return listOfPosts.Where(x => x.User.Id == user.Id)
                    .Where(x => x.Title.ToLower()
                    .Contains(searchString.ToLower()))
                    .OrderBy(x => x.TimeStamp)
                    .Take(limit.Value).ToList();
            }

            var postList = await _alumniNetworkDbContext.Posts
                    .Include(x => x.Group)
                    .Include(x => x.Topic)
                    .Include(x => x.RepliedPosts)
                    .ToListAsync();

            if(user == null)
            {
                return null;
            }

            return postList.Where(x => x.SenderId == user.Id).OrderBy(x => x.TimeStamp).ToList();

        }

        /// <summary>
        /// Method to get all posts between user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="searchString"></param>
        /// <param name="limit"></param>
        /// <returns>Returns a list of posts depending on parameters</returns>
        public async Task<IEnumerable<Post>> GetAllPostBetweenUserAsync(int userId, string? searchString, int? limit)
        {
            var user = await _alumniNetworkDbContext.Users.FindAsync(userId);
            if (searchString != null)
            {
                var postUserSearchString = await _alumniNetworkDbContext.Posts
                    .Where(x => x.Title.ToLower()
                    .Contains(searchString.ToLower()))
                    .Where(x => x.UserId == user.Id).ToListAsync();

                return postUserSearchString;

            }

            if (limit != null)
            {
                var postUserLimit = await _alumniNetworkDbContext.Posts.Where(x => x.UserId == user.Id).ToListAsync();
                return postUserLimit.OrderBy(x => x.Id)
                    .Take(limit.Value)
                    .ToList();
            }

            if (searchString != null && limit != null)
            {
                var postUserBoth = await _alumniNetworkDbContext.Posts
                    .Where(x => x.Title.ToLower()
                    .Contains(searchString.ToLower()))
                    .Where(x => x.UserId == user.Id).ToListAsync();

                return postUserBoth.OrderBy(x => x.Id)
                    .Take(limit.Value)
                    .ToList();
            }
            var messagesRecieved = await _alumniNetworkDbContext.Posts.Where(x => x.UserId == user.Id).ToListAsync();
            return messagesRecieved;
        }

        /// <summary>
        /// Method to get all posts between specific users
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="requestingUserId"></param>
        /// <param name="searchString"></param>
        /// <param name="limit"></param>
        /// <returns>Returns a list of posts between users depending on userId and requestingUserId</returns>
        public async Task<IEnumerable<Post>> GetAllPostsBetweenSpecificUsersAsync(int userId, int requestingUserId, string? searchString, int? limit)
        {
            var user = await _alumniNetworkDbContext.Users.FindAsync(requestingUserId);
            if (searchString != null)
            {
                var postUserSearchString = await _alumniNetworkDbContext.Posts
                    .Where(x => x.Title.ToLower()
                    .Contains(searchString.ToLower()))
                    .Where(x => x.SenderId == userId).Where(x => x.UserId == requestingUserId).ToListAsync();

                return postUserSearchString;

            }

            if (limit != null)
            {
                var postUserLimit = await _alumniNetworkDbContext.Posts.Where(x => x.SenderId == userId).Where(x => x.UserId == requestingUserId).ToListAsync();
                return postUserLimit.OrderBy(x => x.Id)
                    .Take(limit.Value)
                    .ToList();
            }

            if (searchString != null && limit != null)
            {
                var postUserBoth = await _alumniNetworkDbContext.Posts
                    .Where(x => x.Title.ToLower()
                    .Contains(searchString.ToLower()))
                    .Where(x => x.SenderId == userId).Where(x => x.UserId == requestingUserId).ToListAsync();

                return postUserBoth.OrderBy(x => x.Id)
                    .Take(limit.Value)
                    .ToList();
            }
            var listOfMessagesFromSpecificUser = await _alumniNetworkDbContext.Posts.Where(x => x.SenderId == userId).Where(x => x.UserId == requestingUserId).ToListAsync();

            return listOfMessagesFromSpecificUser;
        }

        /// <summary>
        /// Method to get all posts in specific topic
        /// </summary>
        /// <param name="topicId"></param>
        /// <param name="userId"></param>
        /// <param name="searchString"></param>
        /// <param name="limit"></param>
        /// <returns>Returns a list of posts in a topic depending on topicId</returns>
        public async Task<IEnumerable<Post>> GetAllPostTopicsAsync(int topicId, int userId, string? searchString, int? limit)
        {
            var user = await _alumniNetworkDbContext.Users.FindAsync(userId);

            var selectedUserFromTopic = await _alumniNetworkDbContext.Topics.Where(x => x.Id == topicId)
                    .Include(x => x.Users.Where(x => x.Id == userId))
                    .FirstOrDefaultAsync();
            if (selectedUserFromTopic == null)
            {
                return null;
            }
            if (selectedUserFromTopic.Users.Count != 0)
            {
                if (searchString != null)
                {
                    var postTopicsSearchString = await _alumniNetworkDbContext.Posts
                        .Where(x => x.Title.ToLower()
                        .Contains(searchString.ToLower()))
                        .Where(x => x.TopicId == topicId)
                        .Include(x => x.RepliedPosts)
                        .ToListAsync();
                    if (userId > 0)
                    {
                        return postTopicsSearchString.Where(x => x.SenderId == userId).ToList();
                    }

                    return postTopicsSearchString;

                }

                if(limit != null)
                {
                    var postTopicsLimit = await _alumniNetworkDbContext.Posts.Where(x => x.TopicId == topicId)
                        .Include(x => x.RepliedPosts).ToListAsync();

                    if (userId > 0)
                    {
                        return postTopicsLimit.Where(x => x.SenderId == userId).OrderBy(x => x.Id).Take(limit.Value).ToList();
                    }

                    return postTopicsLimit.OrderBy(x => x.Id)
                        .Take(limit.Value)
                        .ToList();
                }

                if(searchString != null && limit != null)
                {
                    var postTopicsBoth = await _alumniNetworkDbContext.Posts
                        .Where(x => x.Title.ToLower()
                        .Contains(searchString.ToLower()))
                        .Where(x => x.TopicId == topicId)
                        .Include(x => x.RepliedPosts)
                        .ToListAsync();

                    if (userId > 0)
                    {
                        return postTopicsBoth.Where(x => x.SenderId == userId).OrderBy(x => x.Id).Take(limit.Value).ToList();
                    }

                    return postTopicsBoth.OrderBy(x => x.Id)
                        .Take(limit.Value)
                        .ToList();
                }
            }
            
            var postTopics = await _alumniNetworkDbContext.Posts.Where(x => x.TopicId == topicId).Include(x => x.RepliedPosts).ToListAsync();
            if(userId > 0)
            {
                return postTopics.Where(x => x.SenderId == userId).ToList();
            }

           
            return postTopics.ToList();
        }

        /// <summary>
        /// Method to get all direct messages
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Returns a list of all direct messages the user has received</returns>
        public async Task<IEnumerable<Post>> GetAllDirectPosts(int userId)
        {
            return await _alumniNetworkDbContext.Posts.Where(x => x.UserId == userId).ToListAsync();
        }

        /// <summary>
        /// Method to get all posts in a specifc group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="searchString"></param>
        /// <param name="limit"></param>
        /// <returns>Returns a list of posts of a group depending on groupId</returns>
        public async Task<IEnumerable<Post>> GetAllPostGroupAsync(int groupId, string? searchString, int? limit)
        {
            // int userId
            //var selectedUserFromGroup = await _alumniNetworkDbContext.Groups.Where(x => x.Id == groupId)
            //        .Include(x => x.Users.Where(x => x.Id == userId))
            //        .FirstOrDefaultAsync();

            //if(selectedUserFromGroup == null)
            //{
            //    return null;
            //}

            //if(selectedUserFromGroup.Users.Count != 0)
            //{
                if (searchString != null)
                {
                    var postGroupSearchString = await _alumniNetworkDbContext.Posts
                        .Where(x => x.Title.ToLower()
                        .Contains(searchString.ToLower()))
                        .Where(x => x.GroupId == groupId)
                        .Include(x => x.RepliedPosts)
                        .ToListAsync();

                    return postGroupSearchString;

                }

                if (limit != null)
                {
                    var postGroupLimit = await _alumniNetworkDbContext.Posts.Where(x => x.GroupId == groupId)
                        .Include(x => x.RepliedPosts)
                        .ToListAsync();

                    return postGroupLimit.OrderBy(x => x.Id)
                        .Take(limit.Value)
                        .ToList();
                }

                if (searchString != null && limit != null)
                {
                    var postGroupBoth = await _alumniNetworkDbContext.Posts
                        .Where(x => x.Title.ToLower()
                        .Contains(searchString.ToLower()))
                        .Where(x => x.GroupId == groupId)
                        .Include(x => x.RepliedPosts)
                        .ToListAsync();

                    return postGroupBoth.OrderBy(x => x.Id)
                        .Take(limit.Value)
                        .ToList();
                }
            //}

            var postGroup = await _alumniNetworkDbContext.Posts.Where(x => x.GroupId == groupId).Include(x => x.RepliedPosts).ToListAsync();

            //return postGroup.Where(x => x.SenderId == userId).ToList();
            return postGroup;
        }

        /// <summary>
        /// Method to get all posts from specific event
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        /// <param name="searchString"></param>
        /// <param name="limit"></param>
        /// <returns>Returns a lists of posts from an event depending on eventId</returns>
        public async Task<IEnumerable<Post>> GetAllPostEventAsync(int eventId, int userId, string? searchString, int? limit)
        {
            var selectedUserFromEvent = await _alumniNetworkDbContext.Events.Where(x => x.Id == eventId)
                    .Include(x => x.Users.Where(x => x.Id == userId))
                    .FirstOrDefaultAsync();

            if(selectedUserFromEvent == null)
            {
                return null;
            }

            if(selectedUserFromEvent.Users.Count != 0)
            {
                if (searchString != null)
                {
                    var postGroupSearchString = await _alumniNetworkDbContext.Posts
                        .Where(x => x.Title.ToLower()
                        .Contains(searchString.ToLower()))
                        .Where(x => x.EventId == eventId)
                        .Include(x => x.RepliedPosts)
                        .ToListAsync();

                    return postGroupSearchString;

                }

                if (limit != null)
                {
                    var postGroupLimit = await _alumniNetworkDbContext.Posts.Where(x => x.EventId == eventId)
                        .Include(x => x.RepliedPosts)
                        .ToListAsync();

                    return postGroupLimit.OrderBy(x => x.Id)
                        .Take(limit.Value)
                        .ToList();
                }

                if (searchString != null && limit != null)
                {
                    var postGroupBoth = await _alumniNetworkDbContext.Posts
                        .Where(x => x.Title.ToLower()
                        .Contains(searchString.ToLower()))
                        .Where(x => x.EventId == eventId)
                        .Include(x => x.RepliedPosts)
                        .ToListAsync();

                    return postGroupBoth.OrderBy(x => x.Id)
                        .Take(limit.Value)
                        .ToList();
                }
            }


            var postEvent = await _alumniNetworkDbContext.Posts.Where(x => x.EventId == eventId).Include(x => x.RepliedPosts).ToListAsync();
            return postEvent.Where(x => x.SenderId == userId).ToList();
        }

        /// <summary>
        /// Creates a new post
        /// </summary>
        /// <param name="post"></param>
        /// <param name="userId"></param>
        /// <returns>Saves and sends the post object to the database if not null</returns>
        public async Task<Post> AddPostAsync(Post post, int userId)
        {
            if (post.GroupId != 0)
            {
                var selectedUserFromGroup = await _alumniNetworkDbContext.Groups.Where(x => x.Id == post.GroupId)
                    .Include(x => x.Users.Where(x => x.Id == userId))
                    .FirstAsync();

                if (selectedUserFromGroup.Users.Count != 0)
                {
                    
                    if (post.EventId == 0)
                    {
                        post.EventId = null;
                    }
                    if (post.TopicId == 0)
                    {
                        post.TopicId = null;
                    }
                    if (post.UserId == 0)
                    {
                        post.UserId = null;
                    }
                    if(post.ReplyId == 0)
                    {
                        post.ReplyId = null;
                    }
                    if (post.ReplyId > 0 || post.ReplyId != null)
                    {
                        var repliedPost = await _alumniNetworkDbContext.Posts.Where(x => x.Id == post.ReplyId).Include(x => x.RepliedPosts).FirstOrDefaultAsync();
                        repliedPost.RepliedPosts.Add(post);
                        await _alumniNetworkDbContext.SaveChangesAsync();
                    }
                    else
                    {
                        _alumniNetworkDbContext.Posts.Add(post);
                        await _alumniNetworkDbContext.SaveChangesAsync();
                    }

                }
                else
                {
                    return post = null;
                }
            }

            else if (post.TopicId != 0)
            {
                var selectedUserFromTopic = await _alumniNetworkDbContext.Topics.Where(x => x.Id == post.TopicId)
                    .Include(x => x.Users.Where(x => x.Id == userId))
                    .FirstAsync();

                if (selectedUserFromTopic.Users.Count != 0)
                {
                    if (post.EventId == 0)
                    {
                        post.EventId = null;
                    }
                    if (post.GroupId == 0)
                    {
                        post.GroupId = null;
                    }
                    if (post.UserId == 0)
                    {
                        post.UserId = null;
                    }
                    if(post.ReplyId == 0)
                    {
                        post.ReplyId = null;
                    }
                    if (post.ReplyId > 0 || post.ReplyId != null)
                    {

                        var repliedPost = await _alumniNetworkDbContext.Posts.Where(x => x.Id == post.ReplyId).Include(x => x.RepliedPosts).FirstOrDefaultAsync();
                        repliedPost.RepliedPosts.Add(post);
                        await _alumniNetworkDbContext.SaveChangesAsync();
                    }
                    else
                    {
                        _alumniNetworkDbContext.Posts.Add(post);
                        await _alumniNetworkDbContext.SaveChangesAsync();
                    }

                    await _alumniNetworkDbContext.SaveChangesAsync();
                }
                else
                {
                    return post = null;
                }
            }

            else if (post.EventId != 0)
            {
                var selectedUserFromEvent = await _alumniNetworkDbContext.Events.Where(x => x.Id == post.EventId)
                    .Include(x => x.Users.Where(x => x.Id == userId))
                    .FirstAsync();

                if (selectedUserFromEvent.Users.Count != 0)
                {
                    if (post.TopicId == 0)
                    {
                        post.TopicId = null;
                    }
                    if (post.GroupId == 0)
                    {
                        post.GroupId = null;
                    }
                    if (post.UserId == 0)
                    {
                        post.UserId = null;
                    }
                    if (post.ReplyId == 0)
                    {
                        post.ReplyId = null;
                    }
                    if (post.ReplyId > 0 || post.ReplyId != null)
                    {
                        var repliedPost = await _alumniNetworkDbContext.Posts.Where(x => x.Id == post.ReplyId).Include(x => x.RepliedPosts).FirstOrDefaultAsync();
                        repliedPost.RepliedPosts.Add(post);
                        await _alumniNetworkDbContext.SaveChangesAsync();
                    }
                    else
                    {
                        _alumniNetworkDbContext.Posts.Add(post);
                        await _alumniNetworkDbContext.SaveChangesAsync();
                    }


                    await _alumniNetworkDbContext.SaveChangesAsync();
                }
                else
                {
                    return post = null;
                }
            }

            else if (post.EventId == 0 && post.TopicId == 0 && post.GroupId == 0 && post.ReplyId == 0 && post.UserId != 0 && post.SenderId != 0)
            {
                _alumniNetworkDbContext.Posts.Add(post);
                if (post.TopicId == 0)
                {
                    post.TopicId = null;
                }
                if (post.GroupId == 0)
                {
                    post.GroupId = null;
                }
                if (post.EventId == 0)
                {
                    post.EventId = null;
                }
                if(post.ReplyId == 0)
                {
                    post.ReplyId = null;
                }

                await _alumniNetworkDbContext.SaveChangesAsync();
            }
            else
            {
                return post = null;
            }

            return post;

        }

        /// <summary>
        /// Updates an post
        /// </summary>
        /// <param name="postId"></param>
        /// <param name="post"></param>
        /// <returns>Saves changes made to the post object and updates it in the database</returns>
        public async Task<Post> UpdatePostAsync(int postId, Post post)
        {
            
            var selectedPost =  await _alumniNetworkDbContext.Posts.Where(x => x.Id == postId).AsNoTracking().FirstOrDefaultAsync();
            if(selectedPost.GroupId != post.GroupId)
            {
                post.GroupId = selectedPost.GroupId;
            }
            if(selectedPost.TopicId != post.TopicId)
            {
                post.TopicId = selectedPost.TopicId;
            }
            if(selectedPost.EventId != post.EventId)
            {
                post.EventId = selectedPost.EventId;
            }
            if(selectedPost.UserId != post.UserId)
            {
                post.UserId = selectedPost.UserId;
            }
            if(selectedPost.SenderId != post.SenderId)
            {
                post.SenderId = selectedPost.SenderId;
            }

               post.TimeStamp = DateTime.Now;
                _alumniNetworkDbContext.Entry(post).State = EntityState.Modified;
                await _alumniNetworkDbContext.SaveChangesAsync();
            

            return post;


        }

        /// <summary>
        /// Gets post replies based on id
        /// </summary>
        /// <param name="replyPostId"></param>
        /// <returns>Returns a list of posts replies based on replyPostId</returns>
        public async Task<IEnumerable<Post>> GetPostRepliesBasedOnId(int replyPostId)
        {
            var repliedPosts = await _alumniNetworkDbContext.Posts
                .SelectMany(x => x.RepliedPosts
                .Where(x => x.Id == replyPostId))
                .Include(x => x.RepliedPosts)
                .ToListAsync();
            return repliedPosts;
        }

        /// <summary>
        /// Method to check if a post exist
        /// </summary>
        /// <param name="postId"></param>
        /// <returns>Retruns the post if it exist</returns>
        public bool PostExists(int postId)
        {
            return _alumniNetworkDbContext.Posts.Any(x => x.Id == postId);
        }
    }
}
