﻿using AlumniNetworkBackendAPI.Models;
using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Services.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections;
using System.Text.RegularExpressions;

namespace AlumniNetworkBackendAPI.Services.Concrete
{
    public class EventService : IEventService
    {
        private readonly AlumniNetworkDbContext _context;
        public EventService(AlumniNetworkDbContext context)
        {
            _context = context;
        }

        public bool EventExists(int id)
        {
            return _context.Events.Any(c => c.Id == id);
        }
        
        /// <summary>
        /// Saves the created event object to the database
        /// </summary>
        /// <param name="events">Object that gets posted to the database</param>
        /// <returns>Saves the current event object to the database</returns>
        public async Task<Event> AddEventAsync(Event events)
        {
            _context.Events.Add(events);
            await _context.SaveChangesAsync();
            return events;
        }

        /// <summary>
        /// Get events based on groupId
        /// </summary>
        /// <param name="groupId">Id of a group to fetch the events its attached to</param>
        /// <returns>Returns a list of events based on groupId</returns>
        public async Task<IEnumerable<Event>> GetAllEventsBasedOnGroup(int groupId)
        {
            //var eventGroup = await _context.Events.Include(x => x.Groups.Where(x => x.Id == groupId)).ToListAsync();v
            var eventGroup = await _context.Events.Include(x => x.Groups).Where(g => g.Groups.Any(x => x.Id == groupId)).ToListAsync();
            return eventGroup;
        }

        /// <summary>
        /// Gets event by Id 
        /// </summary>
        /// <param name="eventId">EventId specifies which event to fetch</param>
        /// <returns>Returns a event based on the eventId</returns>

        public async Task<IEnumerable<Event>> GetEventsBasedOnId(int eventId)
        {
            return await _context.Events.Where(x => x.Id == eventId).ToListAsync();
        }


        /// <summary>
        /// Gets all events included related data based on userId
        /// </summary>
        /// <param name="userId">User id to pass in as paramter when calling method</param>
        /// <returns>Returns a list of events based on userId</returns>
        public async Task<IEnumerable<Event>> GetAllEventsBasedOnUserIdAsync(int userId)
        {
            return await _context.Events.Where(e => e.UserId == userId)
                .Include(x => x.Groups)
                .Include(x => x.Topics)
                .Include(x => x.Users)
                .Include(x => x.Posts)
                .Include(x => x.User)
                .ToListAsync();
        }

        /// <summary>
        /// Get request for events which the user are subscribed to depending on topic or group
        /// </summary>
        /// <param name="id">User id for fetching events</param>
        /// <returns>Returns a list of events based on topics and groups the user are subscribed to</returns>
        public async Task<IEnumerable<Event>> GetSubscribedEventsForUserAsync(int id)
        {
            var showGroupEvents = await _context.Events
                .Include(t => t.Topics)
                .Include(g => g.Groups)
                .Include(u => u.Users)
                .Where(g => g.Groups.Any(e => e.Users.Any(e => e.Id == id)))
                .ToListAsync();

            var showTopicEvents = await _context.Events
                .Include(t => t.Topics)
                .Include(g => g.Groups)
                .Include(u => u.Users)
                .Where(t => t.Topics.Any(e => e.Users.Any(e => e.Id == id)))
                .ToListAsync();

            return showGroupEvents.Union(showTopicEvents).ToList();

        }

        /// <summary>
        /// Updates an event
        /// </summary>
        /// <param name="events">Targeted event object to update</param>
        /// <returns>Updates and saves the event object</returns>
        public async Task UpdateEventAsync(Event events)
        {
            if (EventExists(events.Id))
            {
                _context.Entry(events).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Creates an invitation to a group for an event
        /// </summary>
        /// <param name="groupId">Id for which group to invite</param>
        /// <param name="eventId">Id for which event the group is getting invited to</param>
        /// <returns>Sends the invitation to the group</returns>
        public Task InviteGroupToEventAsync(int groupId, int eventId)
        {
            var eventInvite = _context.Events
                 .Include(g => g.Groups)
                 .Single(e => e.Id == eventId);

            var invitedGroup = _context.Groups
                .Single(e => e.Id == groupId);

            eventInvite.Groups.Add(invitedGroup);

            _context.SaveChanges();
            return Task.CompletedTask;
        }

        /// <summary>
        /// Creates an invitation to the user for an event
        /// </summary>
        /// <param name="userId">Id for which user to invite</param>
        /// <param name="eventId">Id for which event the user is getting invited to</param>
        /// <returns>Sends the invitation to the user</returns>
        public Task InviteUserToEventAsync(int userId, int eventId)
        {
            var eventInvite = _context.Events
                 .Include(g => g.Users)
                 .Single(e => e.Id == eventId);

            var invitedUser = _context.Users
                .Single(e => e.Id == userId);

            eventInvite.Users.Add(invitedUser);

            _context.SaveChanges();
            return Task.CompletedTask;
        }

        /// <summary>
        /// Creates an invitation to the topic towards an event
        /// </summary>
        /// <param name="topicId">Id for which topic to invite</param>
        /// <param name="eventId">Id for which event the topic is getting invited to</param>
        /// <returns>Applies the topic to the event</returns>
        public Task InviteTopicToEventAsync(int topicId, int eventId)
        {
            var eventInvite = _context.Events
                .Include(g => g.Topics)
                .Single(e => e.Id == eventId);

            var invitedTopic = _context.Topics
                .Single(e => e.Id == topicId);

            eventInvite.Topics.Add(invitedTopic);

            _context.SaveChanges();
            return Task.CompletedTask;
        }

        /// <summary>
        /// Removes the invitation towards the event for the group
        /// </summary>
        /// <param name="groupId">Id for which group to remove the invitation</param>
        /// <param name="eventId">Id for which event invitation to remove</param>
        /// <returns>Removes the invite for the group</returns>
        public Task RemoveInviteGroupAsync(int groupId, int eventId)
        {
            var removeEvent = _context.Events
            .Include(g => g.Groups)
            .Where(e => e.Id == eventId)
            .First();

            var removeGroup = _context.Groups
                .Single(e => e.Id == groupId);

            removeEvent.Groups.Remove(removeGroup);
            _context.SaveChanges();
            return Task.CompletedTask;
        }

        /// <summary>
        /// Removes the invitation towards the event for the user
        /// </summary>
        /// <param name="userId">Id for which user to remove the invitation</param>
        /// <param name="eventId">Id for which event invitation to remove</param>
        /// <returns>Removes the invite for the user</returns>
        public Task RemoveInviteUserAsync(int userId, int eventId)
        {
            var removeEvent = _context.Events
            .Include(g => g.Users)
            .Where(e => e.Id == eventId)
            .First();

            var removeUser = _context.Users
                .Single(e => e.Id == userId);

            removeEvent.Users.Remove(removeUser);
            _context.SaveChanges();
            return Task.CompletedTask;
        }

        /// <summary>
        /// Removes the invitation for the topic towards the event
        /// </summary>
        /// <param name="topicId">Id for which topic to remove the invitation</param>
        /// <param name="eventId">Id for which event invitation to remove</param>
        /// <returns>Removes the invite for topic from the event</returns>
        public Task RemoveInviteTopicAsync(int topicId, int eventId)
        {
            var removeEvent = _context.Events
            .Include(g => g.Topics)
            .Where(e => e.Id == eventId)
            .First();

            var removeTopic = _context.Topics
                .Single(e => e.Id == topicId);

            removeEvent.Topics.Remove(removeTopic);
            _context.SaveChanges();
            return Task.CompletedTask;
        }

        /// <summary>
        /// Creates an post with the amount of users attending an event
        /// </summary>
        /// <param name="eventId">Id for specific event</param>
        /// <param name="userId">Id for which user that have attended</param>
        /// <returns>Returns a post with the amount of user for specific event </returns>
        public async Task RSVPRecordPost(int eventId, int userId)
        {
            RSVP rsvp = new()
            {
                EventId = _context.Events.Where(e => e.Id == eventId).Select(e => e.Id).FirstOrDefault(),
                GuestCount = _context.Events.Where(e => e.Id == eventId).Select(e => e.Users.Count).FirstOrDefault(),
                UserId = (int)_context.Users.Where(u => u.Id == userId).Select(u => u.Id).FirstOrDefault()
            };
            _context.RSVPS.Add(rsvp);
            await _context.SaveChangesAsync();
        }
    }
}
