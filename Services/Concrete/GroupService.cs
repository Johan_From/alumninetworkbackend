﻿using AlumniNetworkBackendAPI.Models;
using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Services.Abstract;
using Microsoft.EntityFrameworkCore;

namespace AlumniNetworkBackendAPI.Services.Concrete
{
    public class GroupService : IGroupService
    {
        private readonly AlumniNetworkDbContext _context;

        public GroupService(AlumniNetworkDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Method for getting all the groups
        /// </summary>
        /// <returns>Returns a list of groups depending on the parameters</returns>
        public async Task<IEnumerable<Group>> GetAllGroupsAsync(string? searchString, int? limit)
        {
            if(searchString != null || searchString == string.Empty)
            {
                return await _context.Groups.Include(x => x.Users).Include(c => c.Events).Where(x => x.Name.Contains(searchString)).ToListAsync();
            }

            if(limit != null)
            {
                return await _context.Groups.Include(x => x.Users).Include(c => c.Events).OrderBy(x => x.Id).Take(limit.Value).ToListAsync();
            }

            if(searchString != null && limit != null)
            {
                return await _context.Groups.Include(x => x.Users).Include(c => c.Events).Where(x => x.Name.Contains(searchString)).OrderBy(x => x.Id).Take(limit.Value).ToListAsync();
            }

            return await _context.Groups.Include(x => x.Users).Include(c => c.Events).ToListAsync();
        }

        /// <summary>
        /// Method for getting a specific group
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a group depending on id</returns>
        public async Task<Group> GetSpecificGroupAsync(int id)
        {
            return await _context.Groups.Include(x => x.Users).Include(c => c.Events).Where(x => x.Id == id).FirstOrDefaultAsync();
        } 

        /// <summary>
        /// Method for adding a group
        /// </summary>
        /// <param name="group"></param>
        /// <returns>Saves and sends the group object to the database</returns>
        public async Task<Group> AddGroupAsync(Group group)
        {
            _context.Groups.Add(group);
            await _context.SaveChangesAsync();
            return group;
        }

        /// <summary>
        /// Adds user to the group
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userIds"></param>
        /// <returns>Adds the specific user based on userId to the specific group depending on id</returns>
        public async Task UpdateUserToGroupAsync(int id, int[] userIds)
        {
            var groupUpdate = await _context.Groups
                .Include(c => c.Users)
                .Where(c => c.Id == id)
                .FirstAsync();

            foreach (var userId in userIds)
            {
                var user = await _context.Users.FindAsync(userId);

                groupUpdate.Users.Add(user);
            }

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Method for updating a group
        /// </summary>
        /// <param name="group"></param>
        /// <returns>Saves the updated group object to the database</returns>
        public async Task UpdateGroupAsync(Group group)
        {
            _context.Entry(group).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Boolean method for checking if character exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool GroupExists(int id)
        {
            return _context.Groups.Any(e => e.Id == id);
        }

        /// <summary>
        /// Method for joining a group
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <returns>User based on userId joins group based on groupId</returns>
        public async Task<IEnumerable<Group>> JoinGroup(int groupId, int userId)
        {
            var user = await _context.Users.FindAsync(userId);

            var selectedGroup = await _context.Groups
                .Include(t => t.Users)
                .Where(x => x.Id == groupId)
                .FirstAsync();
            selectedGroup.Users.Add(user);
            await _context.SaveChangesAsync();

            return _context.Groups.Where(x => x.Id == groupId);

        }
    }
}
