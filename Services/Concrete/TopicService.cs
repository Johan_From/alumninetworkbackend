﻿using AlumniNetworkBackendAPI.Models;
using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Services.Abstract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AlumniNetworkBackendAPI.Services.Concrete
{
    public class TopicService : ITopicService
    {
        private readonly AlumniNetworkDbContext _alumniNetworkDbContext;

        public TopicService(AlumniNetworkDbContext alumniNetworkDbContext)
        {
            _alumniNetworkDbContext = alumniNetworkDbContext;
        }

       /// <summary>
       /// Gets all topics based on parameters, if parameters is omitted the entire 
       /// list of topics is returned
       /// </summary>
       /// <param name="searchString">Searchstring that searches topic by name</param>
       /// <param name="limit">Limit that determines how many topics should be presented</param>
       /// <returns>Returns a list of all topics based on parameters</returns>
        public async Task<IEnumerable<Topic>> GetAllTopicsAsync(string searchString, int? limit)
        {
            if(searchString != null)
            {
                return await _alumniNetworkDbContext.Topics
                    .Include(x => x.Users)
                    .Include(x => x.Events)
                    .Where(x => x.Name.ToLower().Contains(searchString.ToLower()))
                    .ToListAsync();
            }

            if(limit != null)
            {
               var listOfTopics = await _alumniNetworkDbContext.Topics
                    .Include(x => x.Users)
                    .Include(x => x.Events)
                    .ToListAsync();
               return listOfTopics.OrderBy(x => x.Id).Take(limit.Value);
               
            }

            if(searchString != null && limit != null)
            {
                return await _alumniNetworkDbContext.Topics.Where(x => x.Name.ToLower().Contains(searchString.ToLower()))
                    .OrderBy(x => x.Id)
                    .Take(limit.Value)
                    .Include(x => x.Users)
                    .Include(x => x.Events)
                    .ToListAsync();
            }

            return await _alumniNetworkDbContext.Topics.Include(x => x.Users).Include(x => x.Events).ToListAsync();
        }

        /// <summary>
        /// Returns one topic based on which id the topic has
        /// </summary>
        /// <param name="id">Id which determines which topic to fetch</param>
        /// <returns>Returns a list of topics based on id</returns>
        public async Task<List<Topic>> GetTopicByIdAsync(int id)
        {
            return await _alumniNetworkDbContext.Topics.Where(x => x.Id == id)
                .Include(x => x.Users)
                .Include(x => x.Events)
                .ToListAsync();
        }

        /// <summary>
        /// Adds a new topic to topics
        /// </summary>
        /// <param name="topic">Topic which is beeing added</param>
        /// <returns>Saves sends the topic object to the database</returns>
        public async Task<Topic> AddTopic(Topic topic)
        {
            _alumniNetworkDbContext.Topics.Add(topic);
            await _alumniNetworkDbContext.SaveChangesAsync();

            return topic;
        }

        /// <summary>
        /// Creates new topic and sets specified user as the first user in that topic
        /// </summary>
        /// <param name="topicId">The topic which is added</param>
        /// <param name="userId">Userid which determines which user to add to topic</param>
        /// <returns>Saves and sends the new topic to database, also adds the creator to the topic</returns>
        public async Task CreateTopicAndAddCreatorAsSubscriber(int topicId, int userId)
        {

            //var user = await _alumniNetworkDbContext.Users.FindAsync(userId);
            //await AddTopic(topic);

            //var listOfTopicUsers = await _alumniNetworkDbContext.Topics
            //    .Include(t => t.Users)
            //    .Where(x => x.Id == topic.Id)
            //    .FirstAsync();
            //listOfTopicUsers.Users.Add(user);

            //await _alumniNetworkDbContext.SaveChangesAsync();

            //return await _alumniNetworkDbContext.Topics
            //    .Where(x => x.Id == topic.Id).ToListAsync();

            var topicUpdate = await _alumniNetworkDbContext.Topics
                .Include(x => x.Users)
                .Where(x => x.Id == topicId)
                .FirstAsync();

            var user = await _alumniNetworkDbContext.Users.FindAsync(userId);
            topicUpdate.Users.Add(user);

            await _alumniNetworkDbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Adds user to topic
        /// </summary>
        /// <param name="topicId">TopicId determines which topic to handle</param>
        /// <param name="userId">UserId determines which user to add to topic</param>
        /// <returns>User based on userId joins topic based on topicId</returns>
        public async Task<IEnumerable<Topic>> JoinTopic(int topicId, int userId)
        {
            var user = await _alumniNetworkDbContext.Users.FindAsync(userId);

            var selectedTopic = await _alumniNetworkDbContext.Topics
                .Include(t => t.Users)
                .Where(x => x.Id == topicId)
                .FirstAsync();
            selectedTopic.Users.Add(user);

            await _alumniNetworkDbContext.SaveChangesAsync();

            return _alumniNetworkDbContext.Topics.Where(x=> x.Id == topicId);

        }

    }
}
