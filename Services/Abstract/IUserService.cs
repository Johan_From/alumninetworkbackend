﻿using AlumniNetworkBackendAPI.Models.Domain;

namespace AlumniNetworkBackendAPI.Services.Abstract
{
    public interface IUserService
    {
        public Task<IEnumerable<User>> GetAllUsersAsync();
        public Task<User> GetSpecificUserAsync(int id);
        public Task<User> GetSpecificUserBasedOnEmailAsync(string email);
        public Task<User> AddUserAsync(User user);
        public Task UpdateUserAsync(User user);
        public bool UserExists(int id);
    }
}
