﻿using AlumniNetworkBackendAPI.Models.Domain;
using System.Collections;

namespace AlumniNetworkBackendAPI.Services.Abstract
{
    public interface IEventService
    {
        public Task<IEnumerable<Event>> GetAllEventsBasedOnUserIdAsync(int userId);
        public Task<IEnumerable<Event>> GetSubscribedEventsForUserAsync(int id);
        public Task<IEnumerable<Event>> GetEventsBasedOnId(int eventId);
        public Task<Event> AddEventAsync(Event events);
        public Task InviteGroupToEventAsync(int groupId, int eventId);
        public Task RemoveInviteGroupAsync(int groupId, int eventId);
        public Task InviteUserToEventAsync(int userId, int eventId);
        public Task RemoveInviteUserAsync(int userId, int eventId);
        public Task InviteTopicToEventAsync(int topicId, int eventId);
        public Task RemoveInviteTopicAsync(int topicId, int eventId);
        public Task UpdateEventAsync(Event events);
        public Task RSVPRecordPost(int eventId, int userId);
        public Task<IEnumerable<Event>> GetAllEventsBasedOnGroup(int groupId);
        public bool EventExists(int id);
    }
}
