﻿using AlumniNetworkBackendAPI.Models.Domain;

namespace AlumniNetworkBackendAPI.Services.Abstract
{
    public interface IGroupService
    {
        public Task<IEnumerable<Group>> GetAllGroupsAsync(string? searchString, int? limit);
        public Task<Group> GetSpecificGroupAsync(int id);
        public Task<Group> AddGroupAsync(Group group);
        public Task UpdateGroupAsync(Group group);
        public Task UpdateUserToGroupAsync(int groupId, int[] userIds);
        public Task<IEnumerable<Group>> JoinGroup(int groupId, int userId);
        public bool GroupExists(int id);
    }
}
