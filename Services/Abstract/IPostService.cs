﻿using AlumniNetworkBackendAPI.Models.Domain;
using Microsoft.AspNetCore.Mvc;

namespace AlumniNetworkBackendAPI.Services.Abstract
{
    public interface IPostService
    {
        public Task<IEnumerable<Post>> GetAllPostsAsync(int userId, string? searchString, int? limit);
        public Task<IEnumerable<Post>> GetAllPostBetweenUserAsync(int userId, string? searchString, int? limit);
        public Task<IEnumerable<Post>> GetAllPostTopicsAsync(int topicId, int userId, string? searchString, int? limit);
        public Task<IEnumerable<Post>> GetAllPostGroupAsync(int groupId, string? searchString, int? limit);
        public Task<IEnumerable<Post>> GetAllPostEventAsync(int topicId, int userId, string? searchString, int? limit);
        public Task<IEnumerable<Post>> GetAllPostsBetweenSpecificUsersAsync(int userId, int requestingUserId, string? searchString, int? limit);
        public Task<IEnumerable<Post>> GetAllDirectPosts(int userId);
        public Task<Post> UpdatePostAsync(int postId, Post post);
        public Task<Post> AddPostAsync(Post post, int userId);
        public Task<IEnumerable<Post>> GetPostRepliesBasedOnId(int replyPostId);
        public bool PostExists(int postId);
    }

}
