﻿using AlumniNetworkBackendAPI.Models.Domain;

namespace AlumniNetworkBackendAPI.Services.Abstract
{
    public interface ITopicService
    {
        public Task<IEnumerable<Topic>> GetAllTopicsAsync(string searchString, int? limit);
        public Task<List<Topic>> GetTopicByIdAsync(int id);
        //public Task<List<Topic>> CreateTopicAndAddCreatorAsSubscriber(Topic topic, int userId);
        public Task CreateTopicAndAddCreatorAsSubscriber(int topicId, int userId);
        public Task<IEnumerable<Topic>> JoinTopic(int topicId, int userId);
        public Task<Topic> AddTopic(Topic topic);


    }
}
