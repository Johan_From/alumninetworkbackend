﻿namespace AlumniNetworkBackendAPI.Models.DTOs.Event
{
    public class EventCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int AllowedGuests { get; set; }
        public string BannerImg { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int InvitedGuests { get; set; }
        public int UserId { get; set; }
    }
}
