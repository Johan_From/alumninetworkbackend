﻿namespace AlumniNetworkBackendAPI.Models.DTOs.User
{
    public class UserReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
        public string WorkStatus { get; set; }
        public string Bio { get; set; }
        public string FunFact { get; set; }
        public List<int> Posts { get; set; }
        public List<int> Events { get; set; }
    }
}
