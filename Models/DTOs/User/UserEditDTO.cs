﻿namespace AlumniNetworkBackendAPI.Models.DTOs.User
{
    public class UserEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
        public string WorkStatus { get; set; }
        public string Bio { get; set; }
        public string FunFact { get; set; }
    }
}
