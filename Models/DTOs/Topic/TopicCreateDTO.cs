﻿namespace AlumniNetworkBackendAPI.Models.DTOs.Topic
{
    public class TopicCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Creator { get; set; }
    }
}
