﻿namespace AlumniNetworkBackendAPI.Models.DTOs.Topic
{
    public class TopicEditDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Creator { get; set; }
    }
}
