﻿namespace AlumniNetworkBackendAPI.Models.DTOs.Group
{
    public class GroupCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Creator { get; set; }
        public bool IsPrivate { get; set; }
    }
}
