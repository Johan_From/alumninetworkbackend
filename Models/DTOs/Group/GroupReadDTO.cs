﻿namespace AlumniNetworkBackendAPI.Models.DTOs.Group
{
    public class GroupReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Creator { get; set; }
        public bool IsPrivate { get; set; }
        public List<int> Users { get; set; }
        public List<int> Events { get; set; }
    }
}
