﻿namespace AlumniNetworkBackendAPI.Models.DTOs.Post
{
    public class PostReadDTO
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int SenderId { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public int TopicId { get; set; }
        public int EventId { get; set; }
        public List<int>? PostReplies { get; set; }
        public int? ReplyId { get; set; }
    }
}
