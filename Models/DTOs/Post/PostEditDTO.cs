﻿namespace AlumniNetworkBackendAPI.Models.DTOs.Post
{
    public class PostEditDTO
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
