﻿using AlumniNetworkBackendAPI.Models.Domain;

namespace AlumniNetworkBackendAPI.Models.Seed
{
    public static class SeedHelper
    {
        public static ICollection<User> GetUserSeeds()
        {
            return new List<User>()
            {
                new User(){Id = 1, Name = "Johan From", Email = "johanfrom@gmail.com", Picture = "img/djksajdks", WorkStatus = "Student at Noroff Accelerate", Bio = "Student who likes to code", FunFact = "Have drives license for boats"},
                new User(){Id = 2, Name = "Armin Ljajic", Email =  "arminljajic@gmail.com", Picture = "img/dhjshdaj", WorkStatus = "Student at Noroff Accelerate", Bio = "Student who likes to code", FunFact = "Is very funny"},
                new User(){Id = 3, Name = "Anders Hansen-Haug", Email = "andershansenhaug@gmail.com", Picture = "img/dhjshdaj", WorkStatus = "Student at Noroff Accelerate", Bio = "Student who likes to code", FunFact = "Is very very funny"}
            };
        }

        public static ICollection<Topic> GetTopicSeeds()
        {
            return new List<Topic>()
            {
                new Topic{Id = 1, Creator = 2, Name = "C#", Description = "Coding in c#"},
                new Topic{Id = 2, Creator = 1,  Name = "JavaScript", Description = "Coding in JavaScript"},
                new Topic{Id = 3, Creator = 1, Name = "Angular", Description = "Working with Angular"},
                new Topic{Id = 4, Creator = 3, Name = "React", Description = "Working with React"}
            };
        }

        public static ICollection<Event> GetEventSeeds()
        {
            return new List<Event>()
            {
                new Event{Id = 1, AllowedGuests = 12, BannerImg = "img/asgasgajj", Description = "A Cool event", EndTime = new DateTime(2022, 09, 05), InvitedGuests = 12, Name = "Eventy", StartTime = DateTime.Now, UserId = 1},
                new Event{Id = 2, AllowedGuests = 15, BannerImg = "img/jdksad", Description = "A mediocre event", EndTime = new DateTime(2022, 09, 05), InvitedGuests = 12, Name = "Eventyty", StartTime = DateTime.Now, UserId = 2}
            };
        }

        public static ICollection<Group> GetGroupSeeds()
        {
            return new List<Group>()
            {
                new Group(){Id = 1, Name = "Accelerate", Creator = 2, Description = "3 months intense education", IsPrivate = true},
                new Group(){Id = 2, Name = "Fullstack", Creator = 1, Description = "Learn everything about fullstack", IsPrivate = false}
            };
        }

        public static ICollection<Post> GetPostSeeds()
        {
            return new List<Post>()
            {
                new Post(){Id = 1, Description = "How interesting", SenderId = 1, TimeStamp = DateTime.Now, Title = "Is this interesting" }
            };
        }
    }
}
