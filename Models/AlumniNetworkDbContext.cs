﻿using AlumniNetworkBackendAPI.Models.Domain;
using AlumniNetworkBackendAPI.Models.Seed;
using Microsoft.EntityFrameworkCore;

namespace AlumniNetworkBackendAPI.Models
{
    public class AlumniNetworkDbContext : DbContext
    {
        public AlumniNetworkDbContext(DbContextOptions<AlumniNetworkDbContext> options) : base(options)
        {

        }

        public DbSet<Event> Events { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<RSVP> RSVPS { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<User> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(SeedHelper.GetUserSeeds());
            modelBuilder.Entity<Topic>().HasData(SeedHelper.GetTopicSeeds());
            modelBuilder.Entity<Event>()
                .HasOne(u => u.User)
                .WithMany(e => e.EventId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Event>().HasData(SeedHelper.GetEventSeeds());
            modelBuilder.Entity<Group>().HasData(SeedHelper.GetGroupSeeds());
            modelBuilder.Entity<Post>()
                .HasOne(u => u.Sender)
                .WithMany(p => p.PostId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Post>().HasData(SeedHelper.GetPostSeeds());
        }
        
    }
}
