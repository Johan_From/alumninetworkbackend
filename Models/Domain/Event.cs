﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlumniNetworkBackendAPI.Models.Domain
{
    public class Event
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(255)]
        public string Name { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }
        public int AllowedGuests { get; set; }
        [MaxLength(500)]
        public string BannerImg { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int InvitedGuests { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<User>? Users { get; set; }
        public ICollection<Group>? Groups { get; set; }
        public ICollection<Topic>? Topics { get; set; }
        public ICollection<Post>? Posts { get; set; }

    }
}
