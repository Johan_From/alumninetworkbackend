﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkBackendAPI.Models.Domain
{
    public class Topic
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(255)]
        public string? Description { get; set; }
        public int Creator { get; set; }
        public ICollection<User>? Users { get; set; }
        public ICollection<Event>? Events { get; set; }


        
    }
}
