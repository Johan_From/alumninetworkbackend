﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkBackendAPI.Models.Domain
{
    public class Group
    {
        public int Id { get; set; }
        [MaxLength(150)]
        public string? Name { get; set; }
        [MaxLength(255)]
        public string? Description { get; set; }
        public int Creator { get; set; }
        public bool IsPrivate { get; set; }
        public ICollection<User>? Users { get; set; }
        public ICollection<Event>? Events { get; set; }
        public ICollection<Post>? Posts { get; set; }
    }
}
