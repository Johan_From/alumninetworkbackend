﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlumniNetworkBackendAPI.Models.Domain
{
    public class User
    {
        public int Id { get; set; }
        [MaxLength(255)]
        public string? Name { get; set; }
        [MaxLength(500)]
        public string Email { get; set; }
        [MaxLength(500)]
        public string? Picture { get; set; }
        [MaxLength(255)]
        public string? WorkStatus { get; set; }
        [MaxLength(255)]
        public string? Bio { get; set; }
        [MaxLength(255)]
        public string? FunFact { get; set; }

        public ICollection<Event> EventId { get; set; }
        public ICollection<Post> PostId { get; set; }
        public ICollection<Post>? Posts { get; set; }
        public ICollection<Group>? Groups { get; set; }
        public ICollection<Topic>? Topics { get; set; }
        public ICollection<Event>? Events { get; set; }
    }
}
