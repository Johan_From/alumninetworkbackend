﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkBackendAPI.Models.Domain
{
    public class Post
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        [Required]
        [MaxLength(255)]
        public string? Title { get; set; }
        [Required]
        [MaxLength(255)]
        public string? Description { get; set; }
        public int? ReplyId { get; set; }
        public virtual Post? Reply { get; set; }
        public virtual ICollection<Post>? RepliedPosts { get; set; }
        public int SenderId { get; set; }
        public User Sender { get; set; }
        public int? UserId { get; set; }
        public  User? User { get; set; }
        public int? GroupId { get; set; }
        public  Group? Group { get; set; }
        public int? TopicId { get; set; }
        public  Topic? Topic { get; set; }
        public int? EventId { get; set; }
        public  Event? Event { get; set; }

    }
}
